def friend(xs):
    return filter(is_friend, xs)

def is_friend(x):
    return len(x) == 4

def test_friends():
    assert friend(["Mark", "Pamela"]) == ["Mark"], 'should be ["Mark"]'
    assert friend(["Mark", "Pamela", "Memo"]) == ["Mark", "Memo"], 'should be ["Mark", "Memo"]'

def test_is_friend():
    assert is_friend("Mark") == True, 'should be true'
    assert is_friend("Pamela") == False, 'should be false'

if __name__ == "__main__":
    test_friends()
    test_is_friend()
    print("Everything passed")

