def is_vowel(x):
    return x in "aeiou"

def count_vowels(xs):
    return len(filter(is_vowel, xs))

def test_is_vowel():
    assert is_vowel("a") == True, "should be True"
    assert is_vowel("e") == True, "should be True"
    assert is_vowel("i") == True, "should be True"
    assert is_vowel("o") == True, "should be True"
    assert is_vowel("u") == True, "should be True"
    assert is_vowel("b") == False, "should be False"

def test_count_vowels():
    assert count_vowels("banana") == 3, "should be 3"
    assert count_vowels("baneni") == 3, "should be 3"
    assert count_vowels("bxnxna") == 1, "should be 1"
    assert count_vowels("bxnxnx") == 0, "should be 0"

if __name__ == "__main__":
    test_is_vowel()
    test_count_vowels()
    print("Everything passed")
