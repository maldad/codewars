def reverse_words(w):
    return " ".join(list(reversed(w.split())))

#-------------TESTS
def test_reverse_words():
    assert reverse_words("Hello world") == "world Hello", 'should be "world Hello"'
    assert reverse_words("Hi there.") == "there. Hi", 'should be "there. Hi"'

if __name__ == "__main__":
    test_reverse_words()
    print("Everything passed")
