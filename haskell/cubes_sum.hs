module SumCubes where
-- Write a function that takes a positive integer n, sums all the cubed values from 1 to n, and returns that sum.
-- Assume that the input n will always be a positive integer.

sumCubes :: Int -> Int
sumCubes n = sum $ map (^ 3) [1..n]

sumCubes' n = n * n * (n + 1) * (n + 1) `div` 4
