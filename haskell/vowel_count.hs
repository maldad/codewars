module VowelCount where

getCount :: String -> Int
getCount = length . filter (\x -> elem x "aeiou")
