module Smiley where

-- Eyes can be marked as : or ;
-- Valid characters for a nose are - or ~
-- Smiling mouth that should be marked with either ) or D.
-- Valid smiley face examples:
-- :) :D ;-D :~)
-- Invalid smiley faces:
-- ;( :> :} :]
-- countSmileys([':)', ';(', ';}', ':-D']); // should return 2;
countSmileys :: [String] -> Int
countSmileys = foldl (\x y -> if elem y valid then succ x else x) 0

valid = [":)", ":D", ":-D", ":~D", ":-)", ":~)", ";)", ";D", ";-D", ";~D", ";-)", ";~)"]

countSmileys' :: [String] -> Int
countSmileys' = length . filter isSmiley

isSmiley :: String -> Bool
isSmiley [e, m] = elem e eyes && elem m mouth
isSmiley [e, n, m] = elem n nose && isSmiley [e, m]

eyes = ":;"
nose = "-~"
mouth = ")D"
