module WordValues where
import Data.Char
-- Given a string "abc" and assuming that each letter 
-- in the string has a value equal to its position in 
-- the alphabet, our string will have a value of 
-- 1 + 2 + 3 = 6. 
-- This means that: a = 1, b = 2, c = 3 ....z = 26.
-- Then you receive a list of words, and you multiplicate
-- each wordValue by its position, this is:
-- wordValue ["abc","abc abc"] should return [6,24]
-- because: "abc" = 6, 6 * 1 = 6
-- and:     "abc abc" = 12, 12 * 2 = 24
wordValue :: [String] -> [Int]
wordValue = zipWith (*) [1..] . map wordValue'

wordValue' :: String -> Int
wordValue' = sum . map alphabet'

alphabet :: Char -> Int
alphabet c
  | c == 'a' = 1
  | c == 'b' = 2
  | c == 'c' = 3
  | c == 'd' = 4
  | c == 'e' = 5
  | c == 'f' = 6
  | c == 'g' = 7
  | c == 'h' = 8
  | c == 'i' = 9
  | c == 'j' = 10
  | c == 'k' = 11
  | c == 'l' = 12
  | c == 'm' = 13
  | c == 'n' = 14
  | c == 'o' = 15
  | c == 'p' = 16
  | c == 'q' = 17
  | c == 'r' = 18
  | c == 's' = 19
  | c == 't' = 20
  | c == 'u' = 21
  | c == 'v' = 22
  | c == 'w' = 23
  | c == 'x' = 24
  | c == 'y' = 25
  | c == 'z' = 26
  | otherwise = 0

alphabet' :: Char -> Int
alphabet' c = if isLetter c then ord c - 96 else 0
